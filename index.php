<?php

trait Hewan
{
    protected $nama;
    protected $darah = 50;
    protected $jumlahKaki;
    protected $keahlian;

    public function atraksi($nama, $keahlian)
    {
        $this->nama = $nama;
        $this->keahlian = $keahlian;
        echo "$this->nama sedang $this->keahlian";
    }
    public function info($nama, $kaki, $keahlian, $attk, $dfnc, $pemain)
    {
        $this->pemain = $pemain;
        $this->nama = $nama;
        $this->kaki = $kaki;
        $this->keahlian = $keahlian;
        $this->attk = $attk;
        $this->dfnc = $dfnc;
        echo "<b> PEMAIN $this->pemain </b> <br> Jenis Hewan : $this->nama <br> Jumlah Kaki : $this->kaki <br> Keahlian : $this->keahlian <br> Attack : $this->attk <br> Defence : $this->dfnc <br>";
    }
}
trait Fight
{
    protected $attackPower;
    protected $defencePower;

    public function serang($nama1, $nama2)
    {

        return "$nama1 sedang menyerang $nama2.  ";
    }
    public function diserang($nama2, $attk, $dfnc)
    {
        $status =  $this->darah - ($attk / $dfnc);
        return "$nama2 sedang diserang <br> darah $nama2 tinggal $status <br> ";
    }
}

class Harimau
{
    use Hewan;
    use Fight;
    public function getInfoHewan()
    {
        return $this->info("Harimau", "4", "Lari Cepat", 7, 8, 1);
    }

    public function menyerang()
    {
        $attk = 7;
        $dfnc = 8;
        echo $this->serang("Harimau", "Elang");
        echo $this->diserang("Elang", $attk, $dfnc);
    }
}
class Elang
{
    use Hewan;
    use Fight;

    public function getInfoHewan()
    {
        return $this->info("Elang", "2", "Terbang Tinggi", 10, 5, 2);
    }
    public function menyerang()
    {
        $attk = 10;
        $dfnc = 5;
        echo $this->serang("Elang", "Harimau");
        echo $this->diserang("Harimau", $attk, $dfnc);
    }
}
$elang = new Elang;
$harimau = new Harimau;

$elang->getInfoHewan();
$harimau->getInfoHewan();

echo '<b> ROUND 1 </b>';
$elang->menyerang();
echo '<b> ROUND 2 </b>';
$harimau->menyerang();
